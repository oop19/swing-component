/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swingcomponent;

import javax.swing.*;  
/**
 *
 * @author a
 */
class Menu {

    JMenu menu, submenu;
    JMenuItem i1, i2, i3, i4, i5;

    Menu() {
        JFrame f = new JFrame("Menu and MenuItem Example");
        JMenuBar mb = new JMenuBar();
        menu = new JMenu("File");
        submenu = new JMenu("Save");
        i1 = new JMenuItem("Home");
        i2 = new JMenuItem("New");
        i3 = new JMenuItem("Open");
        i4 = new JMenuItem("Save");
        i5 = new JMenuItem("Save As");
        menu.add(i1);
        menu.add(i2);
        menu.add(i3);
        submenu.add(i4);
        submenu.add(i5);
        menu.add(submenu);
        mb.add(menu);
        f.setJMenuBar(mb);
        f.setSize(400, 400);
        f.setLayout(null);
        f.setVisible(true);
    }

    public static void main(String args[]) {
        new Menu();
    }
}
